import React, { Component } from 'react'
import PropTypes from 'prop-types'
import InvoiceItem from '../invoice-item'
import './invoice-list.css'
import { TAX_AMOUNT } from '../../../utils/constants'

class InvoiceList extends Component {
	render() {
		return (
			<div className='invoice-list'>
				<div className='list-header'>
					<div>Item</div>
					<div>Quantity</div>
					<div>Price</div>
					<div>Total</div>
				</div>

				{this.props.items.map((item, index) => (
					<InvoiceItem key={index}
					             id={index}
					             item={item}
					             onEdit={this.props.onEditItem}
					             onDelete={() => this.props.onDeleteItem(index)}
					/>
				))}

				<button className='add-item' onClick={this.props.onAddItem}>
					Add Item
				</button>

				<div className='invoice-summary'>
					<div className='summary-headers'>
						<div>Subtotal</div>
						<div>Tax ({TAX_AMOUNT * 100}%)</div>
						<div>Total</div>
					</div>

					<div>
						<span>$ {this.props.subtotal}</span>
						<span>$ {(this.props.subtotal * TAX_AMOUNT).toFixed(2)}</span>
						<span>$ {(this.props.subtotal * (1 + TAX_AMOUNT)).toFixed(2)}</span>
					</div>
				</div>
			</div>
		)
	}
}

InvoiceList.propTypes = {
	items: PropTypes.arrayOf(PropTypes.object),
	onAddItem: PropTypes.func,
	onDeleteItem: PropTypes.func,
	onEditItem: PropTypes.func
}

export default InvoiceList