How to submit:

1. Start an empty local git repo/branch and commit the code found in bitbucket.org/itglue/invoice-editor/branches/master as the initial commit

2. Build the application as per mentioned below

3. Push your code/commits somewhere and email us the link where we can easily view the diff, or download the code


Part 1:
Build an invoice editor in React that allows a user add, edit, or remove line items according to the text-based mockup below. Pricing should be updated on-the-fly as line items are added or edited. Don't worry about persisting invoices. Don't worry about styling.
```
Item                      Qty     Price       Total
--------------------------------------------------------
Widget                   [ 2 ]  [ $10.00 ]  [ $20.00 ] x
Cog                      [ 2 ]  [ $15.99 ]  [ $31.98 ] x
[ New Item ]             [   ]  [        ]  [        ]
                                    --------------------
                                    Subtotal      $51.98
                                    Tax (5%)       $2.60
                                    Total         $54.58
                                    --------------------
```

Part 2:
Incorporate Redux into your application

Part 3:
Write Tests using the test framework of your choice

Part 4:
Make it beautiful and mobile responsive


Instructions to Run
-
1. Check out this branch
2. Run `npm install` on the root of the project
3. Run `npm start` on the root of the project
4. Navigate to http://localhost:3000 if it does not open automatically

Instructions to run the unit tests
-
1. Run `npm test` or `npm run -- --coverage` from the root of the project. 
2. Open `index.html` on the generated `/coverage/lcov-report` folder on your browser.
