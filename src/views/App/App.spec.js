import React from 'react'
import { App } from './App'
import toJson from 'enzyme-to-json'

describe('App presentational tests', () => {

	let props
	beforeEach(() => {
		props = {
			subtotal: 0,
			items: [],
			addItem: jest.fn(),
			deleteItem: jest.fn(),
			editItem: jest.fn()
		}
	})

	it('should render the component', () => {
		const wrapper = shallow(<App {...props}/>)
		expect(toJson(wrapper)).toMatchSnapshot()
	})

	describe('handleItemChange', () => {
		it('should call editItem', () => {
			const wrapper = shallow(<App {...props}/>)
			wrapper.instance().handleItemChange('price', 10, 0)

			expect(props.editItem).toBeCalledWith('price', 10, 0)
		})
	})
})