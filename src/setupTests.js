import { configure, shallow, mount, render } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'

configure({ adapter: new Adapter() })

global.mount = mount
global.render = render
global.shallow = shallow

const mockStore = () => {
	return configureMockStore([thunk])()
}

export { mockStore }