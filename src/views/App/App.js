import React from 'react';
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import InvoiceList from '../components/invoice-list/invoice-list'

import logo from '../../assets/itglue-logo.png';
import './App.css';
import { getInvoiceItems, getInvoiceSubtotal } from '../../reducers'
import { invoiceActions } from '../../redux-modules/invoice'

export class App extends React.Component {


	handleItemChange = (key, value, index) => {
		this.props.editItem(key, value, index)
	}

	render() {
		return (
			<div className='App'>
				<div className='App-header'>
					<img src={logo} className='App-logo' alt='logo'/>
					<h1>Welcome to Invoice Editor</h1>
				</div>

				<InvoiceList
					items={this.props.items}
					onAddItem={this.props.addItem}
					onDeleteItem={this.props.deleteItem}
					onEditItem={this.handleItemChange}
					subtotal={this.props.subtotal}
				/>
			</div>
		);
	}
}

App.propTypes = {
	subtotal: PropTypes.number,
	items: PropTypes.arrayOf(PropTypes.object),
	addItem: PropTypes.func,
	deleteItem: PropTypes.func,
	editItem: PropTypes.func
}

const mapStateToProps = (state) => ({
	subtotal: getInvoiceSubtotal(state),
	items: getInvoiceItems(state)
})

const mapDispatchToProps = {
	addItem: invoiceActions.addItem,
	deleteItem: invoiceActions.deleteItem,
	editItem: invoiceActions.editItem
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
