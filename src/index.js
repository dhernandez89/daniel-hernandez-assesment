import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import App from './views/App/App';
import store from './store';
import './index.css';

const rootElement = document.getElementById('root')

function render(Component) {
	ReactDOM.render(
		<Provider store={store}>
			<Component />
		</Provider>,
		rootElement
	)
}

render(App)
