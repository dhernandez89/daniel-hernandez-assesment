import { combineReducers } from 'redux'
import { ITEM_ADD, ITEM_DELETE, ITEM_EDIT } from './action-types'


const items = (state = [], { type, payload }) => {
	switch (type) {
		case ITEM_ADD:
			return  [...state, { ...payload }]
		case ITEM_EDIT:
			return state.map((item, index) =>
				index === payload.index ? { ...item, [payload.key]: payload.value } : item
			)
		case ITEM_DELETE:
			return state.filter( item => item !== state[payload]  )
		default:
			return state
	}
}

const invoice =  combineReducers({
	items
})

export default invoice

const sumTotals = (total, item) => total + (Number(item.price) * Number(item.quantity))

export const getSubtotal = state => state.items.reduce(sumTotals, 0)
export const getItems = state => state.items