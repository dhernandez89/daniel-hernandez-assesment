import { ITEM_ADD, ITEM_DELETE, ITEM_EDIT } from './action-types'

export const addItem = () => dispatch => {
	dispatch({
		type: ITEM_ADD,
		payload: {
			name: '',
			quantity: '',
			price: ''
		}
	})
}

export const deleteItem = (index) => dispatch => {
	dispatch({
		type: ITEM_DELETE,
		payload: index
	})
}

export const editItem = (key, value, index) => dispatch => {
	dispatch({
		type: ITEM_EDIT,
		payload: { key, value, index }
	})
}